﻿namespace PacotesDeViagens
{
    partial class FormApagarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.ComboBoxApagar = new System.Windows.Forms.ComboBox();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.BackColor = System.Drawing.Color.Transparent;
            this.ButtonApagar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.basket_128;
            this.ButtonApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonApagar.Location = new System.Drawing.Point(482, 271);
            this.ButtonApagar.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(118, 54);
            this.ButtonApagar.TabIndex = 31;
            this.ButtonApagar.UseVisualStyleBackColor = false;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // ComboBoxApagar
            // 
            this.ComboBoxApagar.AllowDrop = true;
            this.ComboBoxApagar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxApagar.FormattingEnabled = true;
            this.ComboBoxApagar.Location = new System.Drawing.Point(42, 39);
            this.ComboBoxApagar.MaxDropDownItems = 10;
            this.ComboBoxApagar.Name = "ComboBoxApagar";
            this.ComboBoxApagar.Size = new System.Drawing.Size(1295, 28);
            this.ComboBoxApagar.TabIndex = 33;
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Close_128;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.Location = new System.Drawing.Point(613, 271);
            this.ButtonSair.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(117, 54);
            this.ButtonSair.TabIndex = 32;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormApagarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1390, 353);
            this.Controls.Add(this.ComboBoxApagar);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonApagar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormApagarPacote";
            this.Text = "Apagar";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.ComboBox ComboBoxApagar;
    }
}