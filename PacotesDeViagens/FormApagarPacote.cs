﻿

namespace PacotesDeViagens
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;

    public partial class FormApagarPacote : Form
    {

        private List<Pacote> listaDePacotesApagar = new List<Pacote>();

                      

        public FormApagarPacote(List<Pacote> listaDePacotes)
        {
            InitializeComponent();

            // lista de pacotes igual a lista que vem do formInicial
            listaDePacotesApagar = listaDePacotes;

            // Datasource para a combobox
            ComboBoxApagar.DataSource = listaDePacotesApagar;

            


            // desabilitar a controlBox
            this.ControlBox = false;
        }
        

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            // variável com o valor do index seleccionado na ComboBox, 
            //equivalente a index na lista de pacotes
            int index = ComboBoxApagar.SelectedIndex;


            // MessageBox de confirmação de Apagar
            var result=MessageBox.Show("\n\n\nTem a certeza que pretende apagar o pacote?\n\n\nId Pacote: " + listaDePacotesApagar[index].IdPacote.ToString() + "\n\nDescrição: " + listaDePacotesApagar[index].DescricaoPacote + "\n\nPreço: " + listaDePacotesApagar[index].Valor.ToString(),"APAGAR", MessageBoxButtons.YesNo,MessageBoxIcon.Warning);

            // Se o butão cancelar for clicado
            if (result == DialogResult.Yes)
            {
                listaDePacotesApagar.RemoveAt(index);



                MessageBox.Show("Pacote apagado com sucesso.","",MessageBoxButtons.OK);

                
                // Refresh da comboBox, primeiro null e depois vai buscar de novo
                ComboBoxApagar.DataSource = null;

                ComboBoxApagar.DataSource = listaDePacotesApagar;
                
               
            }



            

        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }



        /// <summary>
        /// Procura um pacote na lista com o id especifico
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Pacote ProcurarPacote(int index)
        {
            foreach (var pacote in listaDePacotesApagar)
            {
                if (pacote.IdPacote == index)
                {
                    return pacote;
                }
            }

            return null;
        }

    }




}
