﻿namespace PacotesDeViagens
{
    partial class FormEditarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.LabelIdPacote = new System.Windows.Forms.Label();
            this.NumericUpDownPreco = new System.Windows.Forms.NumericUpDown();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.ButtonMenos = new System.Windows.Forms.Button();
            this.ButtonMais = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPreco)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(271, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(246, 164);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "€";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 74);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Descrição";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 166);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Preço";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(110, 68);
            this.TextBoxDescricao.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextBoxDescricao.Multiline = true;
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(368, 64);
            this.TextBoxDescricao.TabIndex = 9;
            this.TextBoxDescricao.TextChanged += new System.EventHandler(this.TextBoxDescricao_TextChanged);
            // 
            // LabelIdPacote
            // 
            this.LabelIdPacote.AutoSize = true;
            this.LabelIdPacote.Location = new System.Drawing.Point(304, 19);
            this.LabelIdPacote.Name = "LabelIdPacote";
            this.LabelIdPacote.Size = new System.Drawing.Size(14, 20);
            this.LabelIdPacote.TabIndex = 12;
            this.LabelIdPacote.Text = "-";
            // 
            // NumericUpDownPreco
            // 
            this.NumericUpDownPreco.DecimalPlaces = 2;
            this.NumericUpDownPreco.Location = new System.Drawing.Point(110, 164);
            this.NumericUpDownPreco.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.NumericUpDownPreco.Name = "NumericUpDownPreco";
            this.NumericUpDownPreco.Size = new System.Drawing.Size(120, 26);
            this.NumericUpDownPreco.TabIndex = 13;
            this.NumericUpDownPreco.ValueChanged += new System.EventHandler(this.NumericUpDownPreco_ValueChanged);
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Close_128;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.FlatAppearance.BorderSize = 0;
            this.ButtonSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSair.Location = new System.Drawing.Point(398, 159);
            this.ButtonSair.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(78, 35);
            this.ButtonSair.TabIndex = 7;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.green_checkmark_and_red_minus_hi2;
            this.ButtonGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonGravar.FlatAppearance.BorderSize = 0;
            this.ButtonGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonGravar.Location = new System.Drawing.Point(311, 159);
            this.ButtonGravar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(79, 35);
            this.ButtonGravar.TabIndex = 6;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // ButtonMenos
            // 
            this.ButtonMenos.BackColor = System.Drawing.Color.Transparent;
            this.ButtonMenos.BackgroundImage = global::PacotesDeViagens.Properties.Resources.left;
            this.ButtonMenos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonMenos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMenos.FlatAppearance.BorderSize = 0;
            this.ButtonMenos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMenos.Location = new System.Drawing.Point(110, 6);
            this.ButtonMenos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonMenos.Name = "ButtonMenos";
            this.ButtonMenos.Size = new System.Drawing.Size(47, 46);
            this.ButtonMenos.TabIndex = 4;
            this.ButtonMenos.Text = "-";
            this.ButtonMenos.UseVisualStyleBackColor = false;
            this.ButtonMenos.Click += new System.EventHandler(this.ButtonMenos_Click);
            // 
            // ButtonMais
            // 
            this.ButtonMais.BackColor = System.Drawing.Color.Transparent;
            this.ButtonMais.BackgroundImage = global::PacotesDeViagens.Properties.Resources.right;
            this.ButtonMais.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonMais.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMais.FlatAppearance.BorderSize = 0;
            this.ButtonMais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMais.Location = new System.Drawing.Point(433, 6);
            this.ButtonMais.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonMais.Name = "ButtonMais";
            this.ButtonMais.Size = new System.Drawing.Size(45, 53);
            this.ButtonMais.TabIndex = 5;
            this.ButtonMais.Text = "+";
            this.ButtonMais.UseVisualStyleBackColor = false;
            this.ButtonMais.Click += new System.EventHandler(this.ButtonMais_Click);
            // 
            // FormEditarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 216);
            this.Controls.Add(this.NumericUpDownPreco);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonGravar);
            this.Controls.Add(this.ButtonMenos);
            this.Controls.Add(this.LabelIdPacote);
            this.Controls.Add(this.ButtonMais);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormEditarPacote";
            this.Text = "FormEditarPacote";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPreco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ButtonMenos;
        private System.Windows.Forms.Button ButtonMais;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.Label LabelIdPacote;
        private System.Windows.Forms.NumericUpDown NumericUpDownPreco;
    }
}