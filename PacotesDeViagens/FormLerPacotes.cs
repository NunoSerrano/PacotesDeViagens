﻿
namespace PacotesDeViagens
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;

    public partial class FormLerPacotes : Form
    {
        // iniciar nova lista para ler
        private List<Pacote> listaDePacotesLER = new List<Pacote>();


        public FormLerPacotes(List<Pacote> listaDePacotes)
        {
            InitializeComponent();

            // lista de pacotes para Ler igual a lista que vem do formInicial
            listaDePacotesLER = listaDePacotes;


            foreach (var pacote in listaDePacotesLER)
            {
                // adicionar linhas com os respectivos dados de cada pacote da lista LER
                DataGridViewLerPacotes.Rows.Add(pacote.IdPacote, pacote.DescricaoPacote, pacote.Valor);
            }

            // desabilitar a controlBox
            this.ControlBox = false;
        }
    

                private void FormLerPacotes_Load(object sender, EventArgs e)
        {
                    
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }

       
    }
}
