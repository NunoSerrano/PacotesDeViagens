﻿

namespace PacotesDeViagens
{

    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;

    public partial class FormCriarPacote : Form
    {
        // iniciar nova lista e pacotes
        public List<Pacote> listaDePacotes= new List<Pacote>();

        //int id = 

        public FormCriarPacote(List<Pacote> listaDePacotes)
        {
            InitializeComponent();
            // lista de pacotes igual a lista que vem do formInicial
            this.listaDePacotes = listaDePacotes;


            // Indica o numero de Id do novo pacote a criar
            LabelIdCriarPacote.Text = GerarIdPacote().ToString();

            // desabilitar a controlBox
            this.ControlBox = false;
        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                MessageBox.Show("Insira uma descricao do Pacote de Viagens");
                return;
            }

            if (NumericUpDownPreco.Value==0)
            {
                MessageBox.Show("Insira um valor para o Pacote de Viagens");
                return;
            }

            // criação do pacote de viagens
            // ID gerado na classe com o valor introduzido igual ao numero da lista do ultimo pacote
            var pacote = new Pacote(GerarIdPacote(), TextBoxDescricao.Text, NumericUpDownPreco.Value) { };

            // adicionar a lista de pacotes
            listaDePacotes.Add(pacote);

            // mensagem a indicar o pacote criado
            // Indicamos o ultimo pacote criado, o ultimo do index da lista (listaDePacotes.Count-1)
            MessageBox.Show("Novo pacote criado com sucesso:\nId Pacote: "+ listaDePacotes[listaDePacotes.Count-1].IdPacote + "\nDEscrição: "+ listaDePacotes[listaDePacotes.Count-1].DescricaoPacote + "\nPreço: "+ listaDePacotes[listaDePacotes.Count-1].Valor + " €");

            LabelIdCriarPacote.Text = GerarIdPacote().ToString();
            TextBoxDescricao.Text=String.Empty;
            NumericUpDownPreco.Value = 0;

        }

        /// <summary>
        /// Gera um numero de id do pacote
        /// </summary>
        /// <returns></returns>
        private int GerarIdPacote()
        {
            return listaDePacotes.Count+1;
        }

        private void FormCriarPacote_Load(object sender, EventArgs e)
        {

        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
