﻿namespace PacotesDeViagens
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.ButtonLer = new System.Windows.Forms.Button();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItemConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemGravar = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuI = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.ButtonOpen = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.BackColor = System.Drawing.Color.Black;
            this.ButtonEditar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Replay_128;
            this.ButtonEditar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonEditar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonEditar.Location = new System.Drawing.Point(182, 36);
            this.ButtonEditar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(74, 61);
            this.ButtonEditar.TabIndex = 4;
            this.ButtonEditar.UseVisualStyleBackColor = false;
            this.ButtonEditar.Click += new System.EventHandler(this.ButtonEditar_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.BackColor = System.Drawing.Color.Black;
            this.ButtonCriar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Add_create_new_more_plus_128;
            this.ButtonCriar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonCriar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCriar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCriar.Location = new System.Drawing.Point(41, 36);
            this.ButtonCriar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(74, 61);
            this.ButtonCriar.TabIndex = 3;
            this.ButtonCriar.UseVisualStyleBackColor = false;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // ButtonLer
            // 
            this.ButtonLer.BackColor = System.Drawing.Color.Black;
            this.ButtonLer.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Data_White;
            this.ButtonLer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonLer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonLer.FlatAppearance.BorderSize = 0;
            this.ButtonLer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLer.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonLer.Location = new System.Drawing.Point(330, 36);
            this.ButtonLer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ButtonLer.Name = "ButtonLer";
            this.ButtonLer.Size = new System.Drawing.Size(74, 61);
            this.ButtonLer.TabIndex = 2;
            this.ButtonLer.UseVisualStyleBackColor = false;
            this.ButtonLer.Click += new System.EventHandler(this.ButtonLer_Click);
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Black;
            this.ButtonSair.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Close_128;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.FlatAppearance.BorderSize = 0;
            this.ButtonSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSair.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonSair.Location = new System.Drawing.Point(506, 510);
            this.ButtonSair.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(53, 47);
            this.ButtonSair.TabIndex = 0;
            this.ButtonSair.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.BackColor = System.Drawing.Color.Black;
            this.ButtonApagar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Trash_Empty_white;
            this.ButtonApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonApagar.FlatAppearance.BorderSize = 0;
            this.ButtonApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonApagar.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonApagar.Location = new System.Drawing.Point(472, 36);
            this.ButtonApagar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(74, 61);
            this.ButtonApagar.TabIndex = 1;
            this.ButtonApagar.UseVisualStyleBackColor = false;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemConfig,
            this.aboutToolStripMenuI});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(593, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ToolStripMenuItemConfig
            // 
            this.ToolStripMenuItemConfig.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemGravar});
            this.ToolStripMenuItemConfig.Name = "ToolStripMenuItemConfig";
            this.ToolStripMenuItemConfig.Size = new System.Drawing.Size(96, 20);
            this.ToolStripMenuItemConfig.Text = "Configurações";
            // 
            // ToolStripMenuItemGravar
            // 
            this.ToolStripMenuItemGravar.Name = "ToolStripMenuItemGravar";
            this.ToolStripMenuItemGravar.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItemGravar.Text = "Gravar";
            // 
            // aboutToolStripMenuI
            // 
            this.aboutToolStripMenuI.Name = "aboutToolStripMenuI";
            this.aboutToolStripMenuI.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuI.Text = "About";
            this.aboutToolStripMenuI.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(40, 522);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(75, 23);
            this.ButtonSave.TabIndex = 6;
            this.ButtonSave.Text = "Save";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // ButtonOpen
            // 
            this.ButtonOpen.Location = new System.Drawing.Point(251, 522);
            this.ButtonOpen.Name = "ButtonOpen";
            this.ButtonOpen.Size = new System.Drawing.Size(75, 23);
            this.ButtonOpen.TabIndex = 7;
            this.ButtonOpen.Text = "Open";
            this.ButtonOpen.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(145, 522);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Save As";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Sings_660x420;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(593, 587);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ButtonOpen);
            this.Controls.Add(this.ButtonSave);
            this.Controls.Add(this.ButtonEditar);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.ButtonLer);
            this.Controls.Add(this.ButtonApagar);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pacotes de Viagens";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Button ButtonLer;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemConfig;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemGravar;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuI;
        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Button ButtonOpen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

