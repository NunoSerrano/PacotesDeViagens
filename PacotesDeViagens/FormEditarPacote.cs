﻿

namespace PacotesDeViagens
{
    using System;
    using System.Windows.Forms;    
    using System.Collections.Generic;
    using Modelos;

    public partial class FormEditarPacote : Form
    {

        private List<Pacote> listaDePacotesEditar = new List<Pacote>();

        // Contador de numero de index da lista
        int index = 0;

        public FormEditarPacote(List<Pacote> listaDePacotes)
        {
            InitializeComponent();
            // lista de pacotes igual a lista que vem do formInicial
            listaDePacotesEditar = listaDePacotes;


            // Indica o numero de Id do primeiro pacote no index da lista
            LabelIdPacote.Text = listaDePacotesEditar[0].IdPacote.ToString();
            
            // Indica a descrição do primeiro pacote no index da lista
            TextBoxDescricao.Text= listaDePacotesEditar[0].DescricaoPacote.ToString();

            // indica o preço do primeiro pacote no index da lista
            NumericUpDownPreco.Value = Convert.ToDecimal(listaDePacotesEditar[0].Valor);

            // desabilitar a controlBox
            this.ControlBox = false;
        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                MessageBox.Show("Insira uma descricao do Pacote de Viagens");
                return;
            }

            if (NumericUpDownPreco.Value == 0)
            {
                MessageBox.Show("Insira um valor para o Pacote de Viagens");
                return;
            }

            // Alteração do pacote de viagens
            // ID mantém-se

            listaDePacotesEditar[index].DescricaoPacote = TextBoxDescricao.Text;
            listaDePacotesEditar[index].Valor = NumericUpDownPreco.Value;

          

            // mensagem a indicar o pacote alterado e as alterações
            
            MessageBox.Show("Novo pacote criado com sucesso:\nId Pacote: " + listaDePacotesEditar[index].IdPacote 
                + "\nDEscrição: " + listaDePacotesEditar[index].DescricaoPacote + "\nPreço: " + listaDePacotesEditar[index].Valor + " €");

            //    LabelIdCriarPacote.Text = GerarIdPacote().ToString();
            //    TextBoxDescricao.Text = String.Empty;
            //    NumericUpDownPreco.Value = 0;

        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonMenos_Click(object sender, EventArgs e)
        {
            // Se ao subtrair 1 valor ficar negativo,volta para o valor máximo do index da lista

            if (index-1<0)
            {
                index = listaDePacotesEditar.Count-1;
            }

            // subtrai um valor a variável que indica o index da lista
            else
            {
                index--;
            }
           

            // Indica o numero de Id do pacote na lista estabelecido pelo no valor index 
            LabelIdPacote.Text = listaDePacotesEditar[index].IdPacote.ToString();

            // Indica a descrição do pacote na lista estabelecido pelo no valor index 
            TextBoxDescricao.Text = listaDePacotesEditar[index].DescricaoPacote.ToString();

            // indica o preço do pacote na lista estabelecido pelo no valor index 
            NumericUpDownPreco.Value = Convert.ToDecimal(listaDePacotesEditar[index].Valor);


        }

        private void ButtonMais_Click(object sender, EventArgs e)
        {
           
            // Se ao adicionar 1 valor ultrapassar o valor máximo do index da lista, volta a 0
            if (index+1>listaDePacotesEditar.Count-1)
            {
                index = 0;
            }

            // adiciona um valor a variável que indica o index da lista
            else
            {
                index++;
            }

            // Indica o numero de Id do pacote na lista estabelecido pelo no valor index 
            LabelIdPacote.Text = listaDePacotesEditar[index].IdPacote.ToString();

            // Indica a descrição do pacote na lista estabelecido pelo no valor index 
            TextBoxDescricao.Text = listaDePacotesEditar[index].DescricaoPacote.ToString();

            // indica o preço do pacote na lista estabelecido pelo no valor index 
            NumericUpDownPreco.Value = Convert.ToDecimal(listaDePacotesEditar[index].Valor);
        }


        private void NumericUpDownPreco_ValueChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxDescricao_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
