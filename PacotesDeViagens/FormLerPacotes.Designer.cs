﻿namespace PacotesDeViagens
{
    partial class FormLerPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridViewLerPacotes = new System.Windows.Forms.DataGridView();
            this.IdPacote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewLerPacotes)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewLerPacotes
            // 
            this.DataGridViewLerPacotes.AllowUserToAddRows = false;
            this.DataGridViewLerPacotes.AllowUserToDeleteRows = false;
            this.DataGridViewLerPacotes.AllowUserToResizeColumns = false;
            this.DataGridViewLerPacotes.AllowUserToResizeRows = false;
            this.DataGridViewLerPacotes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DataGridViewLerPacotes.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.DataGridViewLerPacotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridViewLerPacotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewLerPacotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdPacote,
            this.Descricao,
            this.Valor});
            this.DataGridViewLerPacotes.GridColor = System.Drawing.SystemColors.Info;
            this.DataGridViewLerPacotes.Location = new System.Drawing.Point(0, 0);
            this.DataGridViewLerPacotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DataGridViewLerPacotes.Name = "DataGridViewLerPacotes";
            this.DataGridViewLerPacotes.ReadOnly = true;
            this.DataGridViewLerPacotes.RowHeadersVisible = false;
            this.DataGridViewLerPacotes.Size = new System.Drawing.Size(768, 231);
            this.DataGridViewLerPacotes.TabIndex = 0;
            // 
            // IdPacote
            // 
            this.IdPacote.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.IdPacote.HeaderText = "ID";
            this.IdPacote.Name = "IdPacote";
            this.IdPacote.ReadOnly = true;
            this.IdPacote.Width = 51;
            // 
            // Descricao
            // 
            this.Descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Descricao.HeaderText = "Descrição";
            this.Descricao.Name = "Descricao";
            this.Descricao.ReadOnly = true;
            this.Descricao.Width = 105;
            // 
            // Valor
            // 
            this.Valor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.Valor.DefaultCellStyle = dataGridViewCellStyle1;
            this.Valor.HeaderText = "Preço";
            this.Valor.Name = "Valor";
            this.Valor.ReadOnly = true;
            this.Valor.Width = 75;
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Close_128;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.FlatAppearance.BorderSize = 0;
            this.ButtonSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSair.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonSair.Location = new System.Drawing.Point(786, 174);
            this.ButtonSair.Margin = new System.Windows.Forms.Padding(5);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(52, 42);
            this.ButtonSair.TabIndex = 1;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormLerPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 230);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.DataGridViewLerPacotes);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormLerPacotes";
            this.Text = "LerPacotes";
            this.Load += new System.EventHandler(this.FormLerPacotes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewLerPacotes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewLerPacotes;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdPacote;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
    }
}