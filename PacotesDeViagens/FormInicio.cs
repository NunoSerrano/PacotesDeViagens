﻿

namespace PacotesDeViagens
{

    // PROBLEMAS

    //o ID deve ser gerado dentro da classe? pode ser gerado fora também para mostrar logo?

    //gravar para ficheiro TXT ao sair fazendo um método
    //Ler Ficheiro TXT ao iniciar fazendo um método
    // verificar tamanho do grid do form LER. Não está a fazer scroll quando colocamos muitas colunas
    //Alterar o método de criação do id. Não deve depender do index mas sim de uma variável que conte sempre e nunca repita o valor.
    // Quando se apaga o ultimo da lista, depois volta a criar com esse numero apagado, o ultimo da lista.


    // RESOLVIDO

    //Prevenir abertura duas vezes do mesmo formulario ---> Colocar a criação dos formularios quando iniciamos o form
    //Nos botões apenas colocamos o form.show
    //Para fazer refresh da combox ao apagar, colocamos a combo com null e depois carregamos de novo a informação da lista


    //*** STATIC ***
    // Método Estático para gerar ID dentro da classe. Depois chamamos o método através da classe e não através do objeto
    // Também podemos fazer uma lista static dentro da classe. Esta lista e método não vão com o objetos criados
    //****



    //FLAG
    //foreach (var pacote in listaDePacotes)
    //{
    //    MessageBox.Show("Id:" + pacote.IdPacote.ToString() + "\nDesc:" + pacote.DescricaoPacote + "\nValor:" + pacote.Valor.ToString());
    //}


    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;
    using System.IO;

    public partial class Form1 : Form
    {
        //private List<Pacote> ListaDePacotes;

        private FormCriarPacote formularioCriarPacote;

        private FormLerPacotes formularioLerPacotes;

        private FormEditarPacote formularioEditarPacote;

        private FormApagarPacote formularioApagarPacote;


        public Form1()
        {
            InitializeComponent();




            
            // desabilitar a controlBox
            this.ControlBox = false;

            
        }





        private void ButtonCriar_Click(object sender, EventArgs e)
        {


            //cria novo formulario do tipo FormCriarPacote

            // mostra novo formulario








            formularioCriarPacote = new FormCriarPacote(ListaDePacotes);
            formularioCriarPacote.Show();

        }

        private void ButtonLer_Click(object sender, EventArgs e)
        {
            //cria novo formulario do tipo FormCriarPacote
            formularioLerPacotes = new FormLerPacotes(ListaDePacotes);
            // mostra novo formulario
            formularioLerPacotes.Show();
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonEditar_Click(object sender, EventArgs e)
        {
            //cria novo formulario do tipo FormEditarPacote
            formularioEditarPacote = new FormEditarPacote(ListaDePacotes);

            // mostra novo formulario EditarPacotes
            formularioEditarPacote.Show();
        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            //cria novo formulario do tipo FormEditarPacote
            formularioApagarPacote = new FormApagarPacote(ListaDePacotes);

            // mostra novo formulario EditarPacotes
            formularioApagarPacote.Show();
        }





        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Versão 1.0.0\nGestão de Pacotes Turisticos\nBy Nuno Serrano");
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {


        }

        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //string ficheiro = @"tvInfo.txt";

            //string linha = canal + ";" + volume;

            //StreamWriter sw = new StreamWriter(ficheiro, false);

            //if (!File.Exists(ficheiro))
            //{
            //    sw = File.CreateText(ficheiro);
            //}

            //sw.WriteLine(linha);
            //sw.Close();
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file|*.txt";
            saveFileDialog1.Title = "Save a text File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();



                StreamWriter sw = new StreamWriter(fs);//se nao existir, grava

                //verficar se existe algum problema


                //foreach (var z in ListaDePacotes)
                //{
                //    string linha = string.Format("{0};{1};{2}", z.IdPacote, z.DescricaoPacote, z.Valor);
                //    sw.WriteLine(linha);



                //}
                sw.Close();


                fs.Close();
            }








        }

    }
}
