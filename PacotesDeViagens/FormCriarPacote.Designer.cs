﻿namespace PacotesDeViagens
{
    partial class FormCriarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelIdCriarPacote = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.NumericUpDownPreco = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPreco)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelIdCriarPacote
            // 
            this.LabelIdCriarPacote.AutoSize = true;
            this.LabelIdCriarPacote.Location = new System.Drawing.Point(137, 30);
            this.LabelIdCriarPacote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelIdCriarPacote.Name = "LabelIdCriarPacote";
            this.LabelIdCriarPacote.Size = new System.Drawing.Size(14, 20);
            this.LabelIdCriarPacote.TabIndex = 0;
            this.LabelIdCriarPacote.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 102);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Descrição";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 232);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Preço";
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescricao.Location = new System.Drawing.Point(141, 102);
            this.TextBoxDescricao.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextBoxDescricao.Multiline = true;
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxDescricao.Size = new System.Drawing.Size(503, 61);
            this.TextBoxDescricao.TabIndex = 4;
            // 
            // NumericUpDownPreco
            // 
            this.NumericUpDownPreco.DecimalPlaces = 2;
            this.NumericUpDownPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumericUpDownPreco.Location = new System.Drawing.Point(141, 232);
            this.NumericUpDownPreco.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumericUpDownPreco.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.NumericUpDownPreco.Name = "NumericUpDownPreco";
            this.NumericUpDownPreco.Size = new System.Drawing.Size(111, 26);
            this.NumericUpDownPreco.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "€";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "ID Pacote:";
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = global::PacotesDeViagens.Properties.Resources.Close_128;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.FlatAppearance.BorderSize = 0;
            this.ButtonSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSair.Location = new System.Drawing.Point(541, 227);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(103, 35);
            this.ButtonSair.TabIndex = 6;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCriar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.green_checkmark_and_red_minus_hi2;
            this.ButtonCriar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ButtonCriar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCriar.FlatAppearance.BorderSize = 0;
            this.ButtonCriar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCriar.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonCriar.Location = new System.Drawing.Point(374, 227);
            this.ButtonCriar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(112, 35);
            this.ButtonCriar.TabIndex = 1;
            this.ButtonCriar.UseMnemonic = false;
            this.ButtonCriar.UseVisualStyleBackColor = false;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // FormCriarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 333);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.NumericUpDownPreco);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.LabelIdCriarPacote);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormCriarPacote";
            this.Text = "CriarPacote";
            this.Load += new System.EventHandler(this.FormCriarPacote_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPreco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelIdCriarPacote;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.NumericUpDown NumericUpDownPreco;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}