﻿using PacotesDeViagens.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacotesDeViagens
{

    public class ListaDePacotes

  
    {
        public static List<Pacote> LoadPacotes()
        {
            List<Pacote> output = new List<Pacote>();


            output.Add(new Pacote { IdPacote = 1, DescricaoPacote = "Barcelona, duas pessoas, 5 noites, hotel 4 estrelas, pensão completa", Valor = Convert.ToDecimal(400.55) });
            output.Add(new Pacote { IdPacote = 2, DescricaoPacote = "Paris, duas pessoas, 5 noites, hotel 4 estrelas, pensão completa", Valor = Convert.ToDecimal(800.65) });
            output.Add(new Pacote { IdPacote = 3, DescricaoPacote = "Amesterdão, duas pessoas, 5 noites, hotel 4 estrelas, pensão completa", Valor = Convert.ToDecimal(600.75) });
            output.Add(new Pacote { IdPacote = 4, DescricaoPacote = "Madrid, duas pessoas, 5 noites, hotel 5 estrelas, pensão completa", Valor = Convert.ToDecimal(400.00) });
            output.Add(new Pacote { IdPacote = 5, DescricaoPacote = "Bali, duas pessoas, 7 noites, hotel 5 estrelas, pensão completa", Valor = Convert.ToDecimal(3455.00) }); ;

            
            return output;
        }


    }
}
