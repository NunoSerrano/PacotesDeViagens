﻿namespace PacotesDeViagens.Modelos
{

    public class Pacote
    {

        #region Atributos

        private int _idpacote;

        private string _descricaopacote;

        private decimal _valor;


        #endregion

        #region Propriedades

        public int IdPacote { get; set; }

        //{
        //    get { return _idpacote; }
        //    set { _idpacote =value+1; }
        //}

        public string DescricaoPacote { get; set; }
        public decimal Valor { get; set; }


        #endregion


        #region Construtores

        // Omissão
        public Pacote() : this(0, null, 0) { }

        // Cópia
        public Pacote(Pacote pacote) : this(pacote.IdPacote, pacote.DescricaoPacote, pacote.Valor) { }

        // Parâmetros
        public Pacote(int id, string descricao, decimal value)
        {
            IdPacote = id;
            DescricaoPacote = descricao;
            Valor = value;
        }


        #endregion





        #region metodos

        /// <summary>
        /// Descrição dos valores do objecto
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Id Pacote: {0}  Decrição: {1} Preço:{2} ", IdPacote, DescricaoPacote, Valor);
        }



        public string PacoteCompleto => $"{IdPacote} {DescricaoPacote} {Valor}";



        #endregion
    }

    // FAZER NO PROGRAMA ALUNOS CRUD
    // HashCode

    //public int IdAluno { get;set; }

    //public string Nome { get; set; }

    //public string Apelido { get; set; }

    //public override int GetHashCode()
    //{
    //    return IdAluno;
    //}

    //Ninguém vai buscar o ID de ALUNO mas sim o HashCode
    // procurar o objeto por hashcode
    //Porque assim não interessa onde está guardado
    // NEste caso so ueu que dou o hassh aproveitando o id de aluno


    //no program

    //    Aluno al = new Aluno();

    //Console.WriteLine(al.GetHashCode());

    //    foreach(var aluno in alunos)...

    //

}
